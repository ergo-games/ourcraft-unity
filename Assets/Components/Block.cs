﻿using Model;
using Unity.Entities;

namespace Components {
	public struct Block: IComponentData {
		public BlockId id;
	}
}
