﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Components {
	public struct PlayerCamera : IComponentData {
		public Vector3 viewRotation;
		public int3 focusedDir;
		public int3 focusedPos;
		public bool isLookingAtBlock;
	}
}
