﻿using Unity.Entities;

namespace Components {
	public struct WorldType: IComponentData {
		public Model.WorldType Value;
	}
}