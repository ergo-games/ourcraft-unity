﻿using Unity.Entities;

namespace Components {
	public struct Inventory: IComponentData {
		public int size;
		public DynamicBuffer<InventoryItem> items;

		public Inventory(int size) {
			this.size = size;
			this.items = new DynamicBuffer<InventoryItem>();
		}
	}
}