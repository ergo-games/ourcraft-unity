﻿using Unity.Entities;

namespace Components {
	public struct DroppedItem: IComponentData {
		public int id;
		public int amount;
		public int life;
		
		public DroppedItem(int id, int amount, int life = 5*60) {
			this.id = id;
			this.amount = amount;
			this.life = life;
		} 
	}
}
