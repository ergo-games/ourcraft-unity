﻿using Unity.Entities;

namespace Components {
	public struct Falling: IComponentData {
		public float airDrag;
	}
}
