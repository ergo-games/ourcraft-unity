﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components {
	public struct Velocity: IComponentData {
		// ReSharper disable once InconsistentNaming
		public float3 Value;
		public float x {
			get => Value.x;
			set => Value.x = value;
		}
		public float y {
			get => Value.y;
			set => Value.y = value;
		}
		public float z {
			get => Value.z;
			set => Value.z = value;
		}
	}
}
