﻿using Unity.Entities;

namespace Components {
	public struct CylinderShape: IComponentData {
		public float radius;
		public float height;
	}
}