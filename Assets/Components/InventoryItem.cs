﻿using Unity.Entities;

namespace Components {
	[InternalBufferCapacity(60)]
	public struct InventoryItem: IBufferElementData {
		public int id;
		public int amount;
	}
}