﻿using Unity.Mathematics;
using Unity.Entities;
using UnityEngine;

namespace Components {
	public struct Moving: IComponentData {
		public float maxSpeed;
		public float acc;
		public float3 heading;
		public float jumpForce;
		public Vector3 lookingAt;
	}
}
