﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Material = UnityEngine.Material;
using Velocity = Components.Velocity;
using World = Unity.Entities.World;

namespace Factories {
	public static class Player {
		private static readonly EntityManager _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

		private static readonly EntityArchetype _archetype = _entityManager.CreateArchetype(
			typeof(Moving),
			typeof(PlayerCamera),
			typeof(Inventory),
			typeof(Translation),
			typeof(Rotation),
			typeof(Velocity),
			typeof(Falling),
			typeof(CylinderShape),
			typeof(WorldType),
			typeof(RenderMesh),
			typeof(LocalToWorld)
		);

		private static readonly RenderMesh _renderMesh;

		static Player() {
			_renderMesh = new RenderMesh {
				mesh = Resources.Load<Mesh>("Meshes/Player"),
				material = Resources.Load<Material>("Materials/Player")
			};
		}

		public static Entity create(Model.WorldType worldType, float3 pos, bool spectate=false) {
			Entity entity = _entityManager.CreateEntity(_archetype);
			_entityManager.SetComponentData(entity, new Moving {maxSpeed=15f, acc=.2f, heading=float3.zero, jumpForce=20f});
			_entityManager.SetComponentData(entity, new PlayerCamera ());
			_entityManager.SetComponentData(entity, new Inventory(40));
			_entityManager.SetComponentData(entity, new Translation {Value=pos});
			_entityManager.SetComponentData(entity, new Rotation {Value=Quaternion.identity});
			_entityManager.SetComponentData(entity, new Velocity{Value = float3.zero});
			_entityManager.SetComponentData(entity, new Falling{airDrag = 0});
			_entityManager.SetComponentData(entity, new CylinderShape{radius = .4f, height = 1.8f});
			_entityManager.SetComponentData(entity, new WorldType{Value = worldType});
			_entityManager.SetSharedComponentData(entity, _renderMesh);
			_entityManager.SetComponentData(entity, new LocalToWorld());
			if (spectate) {
				_entityManager.AddComponent<Spectate>(entity);
			}
			return entity;
		}
	}
}
