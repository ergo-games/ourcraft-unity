﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using World = Unity.Entities.World;

namespace Factories {
	public static class Drop {
		private static readonly EntityManager _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

		private static readonly EntityArchetype _archetype = _entityManager.CreateArchetype(
			typeof(Translation), 
			typeof(Scale),
			typeof(Rotation),
			typeof(LocalToWorld),
			typeof(RenderMesh),
			typeof(DroppedItem),
			typeof(RenderBounds)
		);
		
		private static readonly float3 _one = new float3(1);
		// toutes les infos sont cense etre dans item (y compris les trucs supplementaires comme enchantement etc)
		public static Entity create(DroppedItem item, int3 pos) {
			Entity entity = _entityManager.CreateEntity(_archetype);
			_entityManager.SetComponentData(entity, new Scale {Value = 0.25f});
			_entityManager.SetComponentData(entity, new Translation {Value = pos});
			_entityManager.SetComponentData(entity, new Rotation {Value = Quaternion.identity});
			_entityManager.SetSharedComponentData(entity, Utils.MeshLoader.get(item.id));
			_entityManager.SetComponentData(entity, item);
			_entityManager.SetComponentData(entity, new RenderBounds {
				Value = new AABB { Center = float3.zero, Extents = new float3(10, 10, 10) }
			});
			return entity;
		}
	}
}
