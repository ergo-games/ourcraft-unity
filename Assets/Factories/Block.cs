﻿using Components;
using Model;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using World = Unity.Entities.World;


namespace Factories {
	public static class Block {
		private static EntityManager _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
		
		public static readonly EntityArchetype archetype = _entityManager.CreateArchetype(
			typeof(Components.Block),
			typeof(Components.WorldType),
			typeof(Translation),
			typeof(Rotation),
			typeof(LocalToWorld),
			typeof(RenderBounds),
			typeof(MeshUpdate)
		);
		
		public static Entity create(BlockId id, int3 pos, Model.WorldType worldType) {
			Entity entity = _entityManager.CreateEntity(archetype);
			_entityManager.SetComponentData(entity, new Components.Block {id = id});
			_entityManager.SetComponentData(entity, new Components.WorldType {Value = worldType});
			_entityManager.SetComponentData(entity, new Translation {Value = pos});
			_entityManager.SetComponentData(entity, new Rotation {Value = Quaternion.identity});
			_entityManager.SetComponentData(entity, new RenderBounds {
				Value = new AABB { Center = float3.zero, Extents = new float3(2, 2, 2) }
			});
			return entity;
		}
	}
}
