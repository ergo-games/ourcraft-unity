﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using World = Unity.Entities.World;
using WorldType = Components.WorldType;

namespace Factories {
	public static class Chunk {
		private static EntityManager _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

		public static readonly EntityArchetype archetype = _entityManager.CreateArchetype(
			typeof(Translation),
			typeof(WorldType),
			typeof(Rotation),
			typeof(LocalToWorld),
			typeof(RenderBounds),
			typeof(MeshUpdate)
		);
		public static Entity create(int2 pos, Model.WorldType worldType) {
			var entity = _entityManager.CreateEntity(archetype);
			_entityManager.SetComponentData(entity, new Components.WorldType {Value = worldType});
			_entityManager.SetComponentData(entity, new Translation {
				Value = new float3(pos.x*Model.Chunk.width, 0, pos.y*Model.Chunk.width)
			});
			return entity;
		}
	}
}
