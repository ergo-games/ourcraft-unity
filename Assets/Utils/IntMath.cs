﻿using Unity.Mathematics;

namespace Utils {
	public class IntMath {
		public static int div(int x, int q) {
			return x >= 0 ? x / q : (x - q + 1) / q;
		}

		public static int3 mod(int3 x, int3 m) {
			return (x % m + m) % m;
		}
	}
}
