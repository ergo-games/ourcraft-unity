﻿using Components;

namespace Utils {
	public static class ItemBuilder {
		public static DroppedItem drop(InventoryItem item) {
			return new DroppedItem {id = item.id, amount = item.amount};
		}
		
		public static DroppedItem drop(string name, int amount) {
			return drop(Utils.MeshLoader.name2Id[name], amount);
		}
		
		public static DroppedItem drop(int id, int amount) {
			return new DroppedItem {id = id, amount = amount};
		}
	}
}
