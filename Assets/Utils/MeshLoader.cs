﻿using System.Collections.Generic;
using System.IO;
using Unity.Rendering;
using UnityEngine;
using UnityEngine.Rendering;

namespace Utils {
	public static class MeshLoader {
		private static readonly Dictionary<int, RenderMesh> _renderMeshes;
		public static readonly Dictionary<string, int> name2Id;
		private const string _materialDir = "Materials/Blocks/";
		private static readonly Mesh _cube = cubeMesh();
		private static Mesh cubeMesh() {
			GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
			Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh; 
			Object.Destroy(gameObject);
			return mesh;
		}
		
		static MeshLoader() {
			_renderMeshes = new Dictionary<int, RenderMesh>();
			name2Id = new Dictionary<string, int>();
			DirectoryInfo dir = new DirectoryInfo("Assets/Resources/" + _materialDir);
			foreach (FileInfo fileMaterial in dir.GetFiles("*.mat")) {
				Debug.Log("Importing `" + fileMaterial.Name + "`...");
				string name = fileMaterial.Name.Substring(0, fileMaterial.Name.Length - 4);
				Material material = Resources.Load<Material>(_materialDir + name);
				int id = name2Id.Count+1;
				name2Id[name.ToLowerInvariant()] = id;
				_renderMeshes[id] = new RenderMesh {
					mesh = _cube,
					material = material,
					castShadows = ShadowCastingMode.On,
					receiveShadows = true,
				};
			}
		}
		
		public static RenderMesh get(string name) {
			return get(name2Id[name]);
		}

		public static RenderMesh get(int id) {
			return _renderMeshes[id];
		}
	} 
}
