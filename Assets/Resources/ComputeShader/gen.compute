﻿#pragma kernel overworld

// NOISE FUNCTIONS
float3 mod289(float3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

float2 mod289(float2 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

float3 permute(float3 x) {
    return mod289((x * 34.0 + 1.0) * x);
}

float3 taylorInvSqrt(float3 r) {
    return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(float2 v) {
    // NOTE: OUTPUT IS IN [-1, 1] WITH MEDIAN 0
    const float4 C = float4(
        0.211324865405187,  // (3.0-sqrt(3.0))/6.0
        0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
        -0.577350269189626,  // -1.0 + 2.0 * C.x
        0.024390243902439
    ); // 1.0 / 41.0
    
    // First corner
    float2 i = floor(v + dot(v, C.yy));
    float2 x0 = v - i + dot(i, C.xx);
    
    // Other corners
    float2 i1;
    i1.x = step(x0.y, x0.x);
    i1.y = 1.0 - i1.x;
    
    // x1 = x0 - i1  + 1.0 * C.xx;
    // x2 = x0 - 1.0 + 2.0 * C.xx;
    float2 x1 = x0 + C.xx - i1;
    float2 x2 = x0 + C.zz;
    
    // Permutations
    i = mod289(i); // Avoid truncation effects in permutation
    float3 p = permute(permute(i.y + float3(0.0, i1.y, 1.0)) + i.x + float3(0.0, i1.x, 1.0));
    
    float3 m = max(0.5 - float3(dot(x0, x0), dot(x1, x1), dot(x2, x2)), 0.0);
    m = pow(m, 4);
    
    // Gradients: 41 points uniformly over a line, mapped onto a diamond.
    // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
    float3 x = 2.0 * frac(p * C.www) - 1.0;
    float3 h = abs(x) - 0.5;
    float3 ox = floor(x + 0.5);
    float3 a0 = x - ox;
    
    // Normalise gradients implicitly by scaling m
    m *= taylorInvSqrt(a0 * a0 + h * h);
    
    // Compute final noise value at P
    float3 g;
    g.x = a0.x * x0.x + h.x * x0.y;
    g.y = a0.y * x1.x + h.y * x1.y;
    g.z = a0.z * x2.x + h.z * x2.y;
    return 130.0 * dot(m, g);
}


// ACTUAL PROGRAM

struct RegNoise {
    float min;
    float max;
	float roughness;
	float freq;
	float offset;
	int details;
};

RWTexture2D<float4> tex_color;
RWTexture2D<float4> tex_height;
RWTexture2D<float4> tex_info;
int x, y;
float ocean_min, ocean_max, ocean_roughness, ocean_freq, ocean_offset;
int ocean_details;
float mountain_min, mountain_max, mountain_roughness, mountain_freq, mountain_offset;
int mountain_details;
float temperature_min, temperature_max, temperature_roughness, temperature_freq, temperature_offset;
int temperature_details;
float humidity_min, humidity_max, humidity_roughness, humidity_freq, humidity_offset;
int humidity_details;


float param_noise(float min, float max, float roughness, float freq, float offset, int details, float2 pos) {
    float amplitude = (max-min)/2;
    float center = (max+min)/2;
    float rmax = center;
    float h = center;
	for (int i = 0; i < details; i++) {
	    rmax += amplitude*pow(roughness, i);
		h += amplitude*pow(roughness, i)*snoise(offset+pos*freq*pow(4, i));
	}
	return h;
}

float ridges(float min, float max, float roughness, float freq, float offset, int details, float2 pos) {
    float amplitude = (max-min)/2;
    float center = (max+min)/2;
    float h = center;
	for (int i = 0; i < details; i++) {
		h += amplitude*pow(roughness, i)*(1-2*abs(snoise(offset+pos*freq*pow(4, i))));
	}
	return h;
}


[numthreads(16,16,1)]
void overworld(uint3 dtid : SV_DispatchThreadID) {
    int2 pos = dtid.xy+int2(x, y);
    // OCEANS PASS
    float vOcean = param_noise(
        ocean_min, ocean_max, ocean_roughness, ocean_freq, 
        ocean_offset, ocean_details, pos
    );
    if (vOcean < 0) {
        tex_color[dtid.xy] = float4(0.1, 0.2, 0.6, 1);
        tex_info[dtid.xy] = float4(0, 0, 0, 0);
    } else {
        // MOUNTAIN PASS
        float vRidges = min(
            exp(6*(ridges(
            mountain_min, mountain_max, mountain_roughness, 0.0003, 
            mountain_offset, 2, pos
            )-1)), 1
        );
        float vMountains = exp(4*(param_noise(
            mountain_min, mountain_max, mountain_roughness, mountain_freq, 
            mountain_offset*sqrt(2), mountain_details, pos
        )-.95));
        
        // (volcano pass, meteor pass and more later)
        // float h = exp(4*(vRidges-0.95));
        float h = vRidges*vMountains;
        /*
        if(h < .05) {
            h = 0;
        }*/
        tex_height[dtid.xy] = float4(h, 0, 0, 0);
        // tex_color[dtid.xy] = float4(h, h, h, 1);
        // CLIMATE PASS
        float vTemperature = param_noise(
            temperature_min, temperature_max, temperature_roughness, temperature_freq, 
            temperature_offset, temperature_details, pos
        )-h*30;
        float vHumidity = max(
            1/(1+exp(2*vOcean)), 
            param_noise(
                humidity_min, humidity_max, humidity_roughness, humidity_freq, 
                humidity_offset, humidity_details, pos
            )
        );
        tex_info[dtid.xy] = float4(1, h, vTemperature, vHumidity);
        
        if (vTemperature < -10) {
            if (vHumidity < .3f) {
                // polar
                tex_color[dtid.xy] = float4(0, 0.6, 1, 1);
            } else if (vHumidity < .7f) {
                // tundra
                tex_color[dtid.xy] = float4(1, .8, .85, 1);
            } else {
                // snow forest
                tex_color[dtid.xy] = float4(1, 1, 1, 1);
            }
        } else if (vTemperature < 5) {
            if (vHumidity < .3f) {
                // plain
                tex_color[dtid.xy] = float4(.6, .8, .5, 1);
            } else if (vHumidity < .7f) {
                // forest
                tex_color[dtid.xy] = float4(.3, .7, .1, 1);
            } else {
                // marsh
                tex_color[dtid.xy] = float4(.4, .3, 0, 1);
            }
        } else if (vTemperature < 15) {
            if (vHumidity < .3f) {
                // savana
                tex_color[dtid.xy] = float4(.8, .5, 0, 1);
            } else if (vHumidity < .7f) {
                // forest
                tex_color[dtid.xy] = float4(.2, .6, 0, 1);
            } else {
                // mangrove
                tex_color[dtid.xy] = float4(.5, .5, 0, 1);
            }
        } else {
            if (vHumidity < .3f) {
                // desert
                tex_color[dtid.xy] = float4(1, .9, .5, 1);
            } else if (vHumidity < .7f) {
                // plain
                tex_color[dtid.xy] = float4(.7, .8, .4, 1);
            } else {
                // tropical
                tex_color[dtid.xy] = float4(.1, .5, 0, 1);
            } 
        }
    }
}
