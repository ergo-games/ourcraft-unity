﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Velocity = Components.Velocity;
using WorldType = Components.WorldType;

namespace Systems {
	public class MovementSpectate : SystemBase {
		protected override void OnUpdate() {
			float dT = Time.DeltaTime;
			Entities.WithAny<Spectate>().ForEach(
				(
					ref Translation translation, ref Moving moving, 
					ref Velocity velocity, in WorldType worldType
				) => {
					// APPLY HEADING 
					var lookDir = new float3(moving.lookingAt.x, 0, moving.lookingAt.z);
					float3 lookHeading = math.normalizesafe(
						moving.heading.z * lookDir
						- moving.heading.x * math.normalizesafe(math.cross(lookDir, new float3(0, 1, 0)))
					);
					velocity.x = lookHeading.x * moving.maxSpeed;
					velocity.z = lookHeading.z * moving.maxSpeed;
					velocity.y = moving.heading.y * moving.jumpForce;
					translation.Value += velocity.Value*dT;
				}
			).Run();
		}
	}
}
