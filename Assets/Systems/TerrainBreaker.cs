﻿using Components;
using Unity.Entities;
using Unity.Transforms;

namespace Systems {
	public class TerrainBreaker: SystemBase {
		protected override void OnUpdate() {
			Entities.ForEach((ref ToBreak _, ref Block block, ref Translation pos) => {
				// Factories.Drop.create(Utils.ItemBuilder.drop(block.id, 1), new int3(pos.Value));
				// Model.World.destroy(new int3(pos.Value));
			}).Schedule();
		}
	}
}
