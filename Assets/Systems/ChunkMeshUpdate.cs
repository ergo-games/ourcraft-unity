﻿using System;
using System.Collections.Generic;
using System.Linq;
using Components;
using Model;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using MeshCollider = Unity.Physics.MeshCollider;
using World = Model.World;
using WorldType = Components.WorldType;

namespace Systems {
	public class ChunkMeshUpdate: SystemBase {
		private EndSimulationEntityCommandBufferSystem _endSimulationEcb;
		protected override void OnCreate() {
			base.OnCreate();
			_endSimulationEcb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}
		
		protected override void OnUpdate() {
			EntityCommandBuffer ecb = _endSimulationEcb.CreateCommandBuffer();
			Entities
				.WithoutBurst()
				.WithName("ChunkMeshUpdate")
				.WithAll<MeshUpdate, RenderBounds>()
				.ForEach((
					Entity entity,
					in Translation translation, in WorldType worldType
				) => {
					var chunkPos = new int3(translation.Value);

					var vertices = new List<float3>();
					var uvs = new List<Vector2>();
					var triangles = new List<int3>();

					var min = new float3(1024);
					var max = float3.zero;
					var index = 0;
					for (var y = 0; y < Chunk.height; y++) {
						for (var z = 0; z < Chunk.width; z++) {
							for (var x = 0; x < Chunk.width; x++) {
								var blockPos = new int3(x, y, z);
								if (addBlockMesh(Universe.worlds[worldType.Value], chunkPos, blockPos, ref index, vertices, triangles, uvs)) {
									var fPos = new float3(blockPos);
									var minMask = new float3(fPos < min);
									var maxMask = new float3((fPos + new float3(1)) > max);
									min = minMask * fPos + (1 - minMask) * min;
									max = maxMask * (fPos + new float3(1)) + (1 - maxMask) * max;
								}
							}
						}
					}
					
					ecb.SetComponent(entity, new RenderBounds { 
						Value = new AABB { Center = (min + max) / 2, Extents = (max - min) / 2, }
					});
					addRenderMeshToEntity(ecb, entity, vertices, triangles, uvs);
					// addPhysicsColliderToEntity(ecb, entity, vertices, triangles);
					ecb.RemoveComponent<MeshUpdate>(entity);
				}
			).Run();
		}
		
		private static void addRenderMeshToEntity(
			EntityCommandBuffer ecb, Entity entity, 
			ICollection<float3> vertices, ICollection<int3> triangles, ICollection<Vector2> uvs
		) {
			var meshVertices = new List<Vector3>(vertices.Select(
				vertex => new Vector3(vertex.x, vertex.y, vertex.z)
			));
					
			var meshTriangles = new List<int>(3 * triangles.Count);
			foreach (var triangle in triangles) {
				meshTriangles.Add(triangle[0]);
				meshTriangles.Add(triangle[1]);
				meshTriangles.Add(triangle[2]);
			}
			
			Mesh mesh = new Mesh {
				vertices = meshVertices.ToArray(),
				triangles = meshTriangles.ToArray(),
				uv = uvs.ToArray(),
			};
			mesh.RecalculateNormals();
			mesh.Optimize();

			ecb.AddSharedComponent(entity, new RenderMesh {
				mesh = mesh, 
				material = Universe.atlas.material,
				castShadows = ShadowCastingMode.On,
				receiveShadows = true,
			});
		}
		
		private void addPhysicsColliderToEntity(
			EntityCommandBuffer ecb, Entity entity,
			IEnumerable<float3> vertices, IEnumerable<int3> triangles
		) {
			ecb.AddComponent(entity, new PhysicsCollider {
				Value = MeshCollider.Create(
					new NativeArray<float3>(vertices.ToArray(), Allocator.Persistent),
					new NativeArray<int3>(triangles.ToArray(), Allocator.Persistent)
				)
			});
		}
		
		private static bool addBlockMesh(
			Model.World world, int3 chunkPos, int3 relPos, ref int index,
			ICollection<float3> vertices, ICollection<int3> triangles, ICollection<Vector2> uvs
		) {
			var blockPos = relPos + chunkPos;
			var id = world[blockPos];
			if (id == BlockId.air) {
				return false;
			}

			var render = false;
			foreach (Face face in Enum.GetValues(typeof(Face))) {
				if (BlockProperties.transparents.Contains(world[blockPos + BlockMesh.checkDir[(int) face]])) {
					addFaceVertices(vertices, face, relPos);
					addFaceUvs(uvs, face, id);
					addFaceTriangles(triangles, index);
					index += 4;
					render = true;
				}
			}

			return render;
		}
		
		private static void addFaceVertices(ICollection<float3> vertices, Face face, int3 pos) {
			var fpos = new float3(pos);
			var index = (int) face;
			vertices.Add(fpos + BlockMesh.vertices[BlockMesh.faces[index, 0]]); // bottom left
			vertices.Add(fpos + BlockMesh.vertices[BlockMesh.faces[index, 1]]); // bottom right
			vertices.Add(fpos + BlockMesh.vertices[BlockMesh.faces[index, 2]]); // top left
			vertices.Add(fpos + BlockMesh.vertices[BlockMesh.faces[index, 3]]); // top right
		}
		
		private static void addFaceUvs(ICollection<Vector2> uvs, Face face, BlockId block) {
			var ((u0, u1), (v0, v1)) = Universe.atlas.uv(block, face);
			uvs.Add(new Vector2(u0, v0)); // bottom left
			uvs.Add(new Vector2(u1, v0)); // bottom right
			uvs.Add(new Vector2(u0, v1)); // top left
			uvs.Add(new Vector2(u1, v1)); // top right
		}
		
		private static void addFaceTriangles(ICollection<int3> triangles, int index) {
			triangles.Add(index + new int3(
				BlockMesh.triangles[0, 0], BlockMesh.triangles[0, 1], BlockMesh.triangles[0, 2]
			));
			triangles.Add(index + new int3(
				BlockMesh.triangles[1, 0], BlockMesh.triangles[1, 1], BlockMesh.triangles[1, 2]
			));
		}
	}
}
