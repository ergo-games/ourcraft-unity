﻿using System.Collections.Generic;
using Components;
using Model;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using World = Model.World;
using WorldType = Model.WorldType;

namespace Systems {
	public class ChunkUpdate: SystemBase {
		private EndSimulationEntityCommandBufferSystem _endSimulationEcb;
		private EntityArchetype _archetype;
		protected override void OnCreate() {
			base.OnCreate();
			_endSimulationEcb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
			_archetype = Factories.Chunk.archetype;
		}
		
		protected override void OnUpdate() {
			EntityCommandBuffer ecb = _endSimulationEcb.CreateCommandBuffer();
			int2 oldChunk = int2.zero;
			WorldType oldWorldType = WorldType.none;
			Entities
				.WithAll<PlayerCamera>()
				.WithoutBurst()
				.ForEach((in Translation pos, in Components.WorldType worldType) => {
				int2 pChunk = new int2(new float2(pos.Value.x, pos.Value.z)) / Chunk.width;
				if (!pChunk.Equals(oldChunk) || worldType.Value != oldWorldType) {
					for (var x = pChunk.x - Settings.renderDistance; x <= pChunk.x + Settings.renderDistance; x++) {
						for (var y = pChunk.y - Settings.renderDistance; y <= pChunk.y + Settings.renderDistance; y++) {
							var chunk = new int2(x, y);
							if (!Universe.worlds[worldType.Value].isLoaded(chunk)) {
								Universe.worlds[worldType.Value].loadChunk(chunk);
								createChunkEntity(ecb, worldType, Chunk.width * new int3(chunk.x, 0, chunk.y));
							}
						}
					}
				}
				oldChunk = pChunk;
				oldWorldType = worldType.Value;
			}).Run();
		}
		
		private void createChunkEntity(EntityCommandBuffer ecb, Components.WorldType worldType, int3 chunkPos) {
			Entity e = ecb.CreateEntity(_archetype);
			ecb.SetComponent(e, new Components.WorldType {Value = worldType.Value});
			ecb.SetComponent(e, new Translation {Value = new float3(chunkPos)});
		}
	}
}
