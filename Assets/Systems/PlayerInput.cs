﻿using System;
using Model;
using Unity.Entities;
using UnityEngine;

namespace Systems {
	public class PlayerInput: ComponentSystem {
		protected override void OnUpdate() {
			PlayerAction.mouseX = Input.GetAxisRaw("Mouse X");
			PlayerAction.mouseY = Input.GetAxisRaw("Mouse Y");
			PlayerAction.attack = Input.GetMouseButtonDown(0);
			PlayerAction.use = Input.GetMouseButtonDown(1);
			PlayerAction.forward = Convert.ToInt32(Input.GetKey(Settings.bindings["forward"]));
			PlayerAction.backward = Convert.ToInt32(Input.GetKey(Settings.bindings["backward"]));
			PlayerAction.left = Convert.ToInt32(Input.GetKey(Settings.bindings["left"]));
			PlayerAction.right = Convert.ToInt32(Input.GetKey(Settings.bindings["right"]));
			PlayerAction.jumping = Convert.ToInt32(Input.GetKey(Settings.bindings["jumping"]));
			PlayerAction.crouching = Convert.ToInt32(Input.GetKey(Settings.bindings["crouching"]));
		}
	}
}
