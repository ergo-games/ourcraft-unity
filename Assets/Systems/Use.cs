﻿using Components;
using Model;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems {
	public class Use: SystemBase {
		private static bool isBlockOnPlayer(int3 blockPos, float3 feetPos) {
			return math.abs(feetPos.x - blockPos.x) < 0.8 
				&& blockPos.y >= feetPos.y && blockPos.y <= feetPos.y+2
				&& math.abs(feetPos.z - blockPos.z) < 0.8;
		}
		
		protected override void OnUpdate() {
			Entities.ForEach((in PlayerCamera playerCamera, in Translation translation) => {
				if (playerCamera.isLookingAtBlock && PlayerAction.use) {
					int3 blockPos = playerCamera.focusedPos + playerCamera.focusedDir;
					if (!isBlockOnPlayer(blockPos, translation.Value)) {
						// Model.World.set(blockPos);
					}
				}
			}).WithoutBurst().Run();
		}
	}
}