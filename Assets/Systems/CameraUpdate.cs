﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Math = System.Math;

namespace Systems {
	[UpdateAfter(typeof(TransformSystemGroup))]
	public class CameraUpdate: SystemBase {
		private static Transform _camera;
		private static readonly float3 _offset = new float3(0f, 1.625f, 0f);
		private static CollisionWorld _collisionWorld;
		private static BuildPhysicsWorld _physicsWorld;
		private static GameObject _outline;
		
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		protected static void start() {
			// ReSharper disable once PossibleNullReferenceException
			_camera = Camera.main.transform;
			Cursor.lockState = CursorLockMode.Locked;
			_outline = GameObject.FindGameObjectWithTag("Outline");
		}

		private static int pointCastNormal(float3 relativePos) {
			float3 focusedAxis = math.abs(relativePos - math.round(relativePos));
			for (int i = 0; i < 3; i++) {
				if (Math.Abs(focusedAxis[i]) < 0.0001f) {
					return i;
				}
			}
			return 0;
		}
		
		protected override void OnUpdate() {
			_physicsWorld = World.GetExistingSystem<BuildPhysicsWorld>();
			_collisionWorld = _physicsWorld.PhysicsWorld.CollisionWorld;
			Entities.WithoutBurst().ForEach((ref PlayerCamera playerCamera, in Translation translation, in Moving moving) => {
				_camera.position = translation.Value + _offset;
				_camera.rotation = Quaternion.Euler(playerCamera.viewRotation);
				var hit = new Unity.Physics.RaycastHit();
				var input = new RaycastInput {
					Start = _camera.position,
					End = _camera.position + moving.lookingAt*4.5f,
					Filter = new CollisionFilter {
						BelongsTo = ~0u,
						CollidesWith = ~1u,
					}
				};
				bool hasIt = _collisionWorld.CastRay(input, out hit);
				if (hasIt) {
					Entity e = _physicsWorld.PhysicsWorld.Bodies[hit.RigidBodyIndex].Entity;
					_outline.transform.position = EntityManager.GetComponentData<Translation>(e).Value;
					playerCamera.focusedPos = new int3(EntityManager.GetComponentData<Translation>(e).Value);
					float3 relativePos = EntityManager.GetComponentData<Translation>(e).Value - hit.Position - new float3(0.5f, 0.5f, 0.5f);
					int axis = pointCastNormal(relativePos);
					playerCamera.focusedDir = int3.zero;
					playerCamera.focusedDir[axis] = relativePos[axis] < 0f ? 1 : -1;
					playerCamera.isLookingAtBlock = true;
					_outline.SetActive(true);
				} else {
					playerCamera.isLookingAtBlock = false;
					_outline.SetActive(false);
				}
			}).Run();
		}
	}
}
