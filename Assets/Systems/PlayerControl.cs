﻿using Components;
using Model;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Systems {
	public class PlayerControl: SystemBase {
		protected override void OnUpdate() {
			Entities.ForEach(
				(ref PlayerCamera playerCamera, ref Moving moving) => {
					moving.heading = math.normalizesafe(new float3(
						PlayerAction.right-PlayerAction.left,
						0,
						PlayerAction.forward-PlayerAction.backward
					), float3.zero);
					moving.heading.y = PlayerAction.jumping-PlayerAction.crouching;
					playerCamera.viewRotation += new Vector3(-PlayerAction.mouseY, PlayerAction.mouseX, 0);
					playerCamera.viewRotation.x = Mathf.Clamp(playerCamera.viewRotation.x, -89.9f, 89.9f);
					moving.lookingAt = math.normalizesafe(
						Quaternion.Euler(playerCamera.viewRotation) * new Vector3(0, 0, 1),
						Vector3.forward
					);
				}
			).WithoutBurst().Run();
		}
	}
}
