﻿using Components;
using Model;
using Unity.Entities;
using WorldType = Components.WorldType;

namespace Systems {
	public class Attack: SystemBase {
		protected override void OnUpdate() {
			Entities
				.WithoutBurst()
				.ForEach((in PlayerCamera playerCamera, in WorldType worldType) => { 
					if (playerCamera.isLookingAtBlock && PlayerAction.attack) {
						// Entity block = Universe.worlds[worldType][playerCamera.focusedPos];
						// World.EntityManager.AddComponent(block, typeof(ToBreak));
					}
				}).Run();
		}
	}
}
