﻿using Components;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

namespace Systems {
	public class DropRotater : SystemBase {
		protected override void OnUpdate() {
			Entities.ForEach((ref Rotation rotation, in DroppedItem item) => {
				rotation.Value = math.mul(rotation.Value, quaternion.RotateY(0.05f));
			}).ScheduleParallel();
		}
	}
}
