﻿using Components;
using Model;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Velocity = Components.Velocity;
using World = Model.World;
using WorldType = Components.WorldType;

namespace Systems {
	public class Movement : SystemBase {
		protected override void OnUpdate() {
			float dT = Time.DeltaTime;
			Entities.WithNone<Spectate>().ForEach(
				(
					ref Translation translation, ref Moving moving, ref Velocity velocity, 
					in Falling falling, in WorldType worldType
				) => {
					// APPLY HEADING 
					var lookDir = new float3(moving.lookingAt.x, 0, moving.lookingAt.z);
					float3 lookHeading = math.normalizesafe(
						moving.heading.z * lookDir
						- moving.heading.x * math.normalizesafe(math.cross(lookDir, new float3(0, 1, 0)))
					);
					velocity.x = lookHeading.x * moving.maxSpeed;
					velocity.z = lookHeading.z * moving.maxSpeed;
					// check if on ground
					BlockId ground = Universe.worlds[worldType.Value][new int3(math.round(translation.Value)) - new int3(0, 1, 0)];
					if (ground != BlockId.air) {
						velocity.y = moving.heading.y * moving.jumpForce;
					} else {
						// apply gravity and airDrag to speed
						velocity.y += (-9.8f + velocity.y*falling.airDrag)*dT;
					}
					
					// APPLY VELOCITY
					translation.Value += velocity.Value*dT;
					
					// CHECK & RESOLVE TERRAIN COLLISION
					// Debug.Log(translation.Value.x + " ; " + translation.Value.z);
				}
				// fixme: se renseigner pour voir si on peut utiliser Burst
			).WithoutBurst().Run();
		}
	}
}
