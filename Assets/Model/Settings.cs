﻿using System.Collections.Generic;
using UnityEngine;

namespace Model {
	public static class Settings {
		public static int seed;
		public static int renderDistance;
		public static readonly Dictionary<string, KeyCode> bindings = new Dictionary<string, KeyCode>();
		public static readonly Dictionary<(MapType, string), float> generation = new Dictionary<(MapType, string), float>();
	}
}
