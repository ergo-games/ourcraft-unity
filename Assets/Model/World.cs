﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Utils;

namespace Model {
	public enum WorldType {
		none = 0,
		overworld,
		nether,
		end,
		aether
	}
	
	public static class Universe {
		public static Dictionary<WorldType, World> worlds = new Dictionary<WorldType, World>();
		public static readonly Atlas atlas = new Atlas("Textures/Blocks/");
	}
	
	public class World {
		public WorldType worldType;
		public GenOverworld gen;
		private readonly Dictionary<int2, Chunk> _chunks = new Dictionary<int2, Chunk>();

		public World(WorldType worldType) {
			this.worldType = worldType;
			gen = new GenOverworld();
			
		}
		
		public BlockId this[int3 pos] {
			get {
				var (chunk, chunkPos) = splitCoords(pos);
				if (!_chunks.ContainsKey(chunk) || pos.y < 0 || pos.y >= Chunk.height) {
					return BlockId.air;
				}
				return _chunks[chunk][chunkPos];
			}
		}
		
		private (int2, int3) splitCoords(int3 pos) {
			int2 chunkCoord = new int2(IntMath.div(pos.x, Chunk.width), IntMath.div(pos.z, Chunk.width));
			int3 blockCoord = IntMath.mod(pos, Chunk.width);
			blockCoord.y = pos.y;
			// Debug.Log($"Pos {pos} -> {chunkCoord}>{blockCoord}");
			return (chunkCoord, blockCoord);
		}
		
		public void genChunks() {
			var chunks = gen.get(
				new int2(-Settings.renderDistance, -Settings.renderDistance),
				(Settings.renderDistance*2+1)*Chunk.width
			);
			foreach (var item in chunks) {
				_chunks[item.Key] = item.Value;
				Factories.Chunk.create(item.Key, worldType);
			}
		}
		
		public void loadChunk(int2 pos) {
			Debug.Log("Load "+pos);
			var chunks = gen.get(pos, Chunk.width);
			foreach (var item in chunks) {
				_chunks[item.Key] = item.Value;
			}
		}
		
		public bool isLoaded(int2 pos) {
			return _chunks.ContainsKey(pos);
		}
	}
}
