﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

namespace Model {
	public static class BlockProperties {
		public static readonly HashSet<BlockId> transparents = new HashSet<BlockId> {BlockId.air, BlockId.glass};
		public static readonly Dictionary<(BlockId, Face), BlockId> texOverride = new Dictionary<(BlockId, Face), BlockId> {
			{(BlockId.grass_block, Face.bot), BlockId.dirt},
			{(BlockId.crafting_table, Face.bot), BlockId.oak_planks},
		};
		public static readonly HashSet<(BlockId, Face?)> overlays = new HashSet<(BlockId, Face?)> {
			(BlockId.grass_block, Face.top),
			(BlockId.oak_leaves, null)
		};
		public static readonly HashSet<BlockId> greens = new HashSet<BlockId> {
			BlockId.grass_block,
		};
	}
	
	public enum BlockId : ushort {
		air = 0,
		bedrock,
		stone,
		granite,
		gravel,
		dirt,
		grass_block,
		oak_planks,
		crafting_table,
		furnace,
		glass,
		sand,
		red_sand,
		oak_log,
		oak_leaves,
		coal_ore,
		iron_ore,
		gold_ore,
		diamond_ore,
		lapis_ore,
		emerald_ore,
	}
	
	public enum Face {
		front = 0,
		back = 1,
		left = 2,
		right = 3,
		top = 4,
		bot = 5,
	}
	
	public enum Orientation: byte {
		north,
		south,
		east,
		west,
		up,
		down,
		none, // if it's not orientable ? not sure to use it
	}
	
	public enum Axis: byte {
		z,
		x,
		y,
		none,
	}

	public class Atlas {
		public readonly Material material;
		private readonly Dictionary<(BlockId, Face), int> _blockTexId;
		private readonly Dictionary<(BlockId, Face), int> _overlayTexId;
		private readonly Rect[] _uvMap;

		public Atlas(string texturesPath) {
			_blockTexId = new Dictionary<(BlockId, Face), int>();
			_overlayTexId = new Dictionary<(BlockId, Face), int>();
			// ReSharper disable once CollectionNeverUpdated.Local
			var blockTexs = new List<Texture2D>();
			var defaults = new Dictionary<string, IEnumerable<Face>> {
				{"", Enum.GetValues(typeof(Face)).Cast<Face>()},
				{"_side", new [] {Face.front, Face.back, Face.left, Face.right}},
				{"_top", new [] {Face.top, Face.bot}}
			};
			// For each kind of block
			foreach (BlockId blockId in Enum.GetValues(typeof(BlockId))) {
				if (blockId == BlockId.air) {
					continue;
				}
				// DEFAULT PASS
				foreach (var def in defaults) {
					var tex = (Texture2D)Resources.Load($"{texturesPath}{blockId}{def.Key}", typeof(Texture2D));
					if (tex) {
						foreach (var face in def.Value) {
							_blockTexId[(blockId, face)] = blockTexs.Count;
						}
						blockTexs.Add(tex);
						var overlay = (Texture2D)Resources.Load($"{texturesPath}{blockId}{def.Key}_overlay", typeof(Texture2D));
						if (overlay) {
							foreach (var face in def.Value) {
								_overlayTexId[(blockId, face)] = blockTexs.Count;
							}
							blockTexs.Add(overlay);
						}
					}
				}
				// FACE PASS
				foreach (Face face in Enum.GetValues(typeof(Face))) {
					// because top was already checked in DEFAULT PASS
					if (face == Face.top) {
						continue;
					}
					var tex = (Texture2D)Resources.Load($"{texturesPath}{blockId}_{face}", typeof(Texture2D));
					if (tex) {
						_blockTexId[(blockId, face)] = blockTexs.Count;
						blockTexs.Add(tex);
						var overlay = (Texture2D)Resources.Load($"{texturesPath}{blockId}_{face}_overlay", typeof(Texture2D));
						if (overlay) {
							_overlayTexId[(blockId, face)] = blockTexs.Count;
							blockTexs.Add(overlay);
						}
					}
				}
			}
			// OVERRIDE PASS
			foreach (var over in BlockProperties.texOverride) {
				BlockId id = over.Key.Item1;
				Face face = over.Key.Item2;
				_blockTexId[(id, face)] = _blockTexId[(over.Value, face)];
			}

			foreach (var overlay in BlockProperties.overlays) {
				BlockId id = overlay.Item1;
				if (overlay.Item2.HasValue) {
					Face face = (Face)overlay.Item2;
					_overlayTexId[(id, face)] = _blockTexId[(id, face)];
				} else {
					foreach (Face face in Enum.GetValues(typeof(Face))) {
						_overlayTexId[(id, face)] = _blockTexId[(id, face)];
					}
				}
			}

			var atlasTex = new Texture2D(2, 2) {
				filterMode = FilterMode.Point, 
				wrapMode = TextureWrapMode.Clamp
			};
			_uvMap = atlasTex.PackTextures(blockTexs.ToArray(), 0);
			material = new Material(Shader.Find("Standard")) {mainTexture = atlasTex, enableInstancing = true, };
			material.SetFloat(Shader.PropertyToID("_Glossiness"), 0f);
		}

		public ((float, float), (float, float)) uv(BlockId id, Face face) {
			Rect rect = _uvMap[_blockTexId[(id, face)]];
			return ((rect.x, rect.x+rect.width), (rect.y, rect.y+rect.height));
		}
	}
	
	public static class BlockMesh {
		public static readonly float3[] vertices = {
			new float3(0f, 0f, 0f),
			new float3(1f, 0f, 0f),
			new float3(1f, 1f, 0f),
			new float3(0f, 1f, 0f),
			new float3(0f, 0f, 1f),
			new float3(1f, 0f, 1f),
			new float3(1f, 1f, 1f),
			new float3(0f, 1f, 1f),
		};

		public static readonly int[,] triangles = {
			{0, 1, 2}, 
			{3, 2, 1}
		};

		public static readonly int[,] faces = {
			{ 4, 5, 7, 6 }, // front
			{ 1, 0, 2, 3 }, // back
			{ 0, 4, 3, 7 }, // left
			{ 5, 1, 6, 2 }, // right
			{ 7, 6, 3, 2 }, // top
			{ 0, 1, 4, 5 }, // bot
		};

		public static readonly int3[] checkDir = {
			new int3(0, 0, 1),
			new int3(0, 0, -1),
			new int3(-1, 0, 0),
			new int3(1, 0, 0),
			new int3(0, 1, 0),
			new int3(0, -1, 0),
		};
	}
}
