﻿using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Model {
	public enum MapType {
		ocean = 0,
		humidity,
		temperature,
		mountain,
		volcano,
		meteor,
		vegetation,
	}
	
	public class GenOverworld {
		private readonly ComputeShader shader_gen;
		private readonly ComputeShader shader_h2n;
		private readonly int kernel_gen;
		private readonly int kernel_h2n;
		
		public GenOverworld() {
			shader_gen = Resources.Load<ComputeShader>("ComputeShader/gen");
			kernel_gen = shader_gen.FindKernel("overworld");
			shader_h2n = Resources.Load<ComputeShader>("ComputeShader/height_to_normal");
			kernel_h2n = shader_h2n.FindKernel("heightToNormal");
		}
		
		private void setStructGPU() {
			Random.InitState(Settings.seed);
			// Ocean
			shader_gen.SetFloat("ocean_min", -1);
			shader_gen.SetFloat("ocean_max", 1);
			shader_gen.SetFloat("ocean_roughness", Settings.generation[(MapType.ocean, "roughness")]);
			shader_gen.SetFloat("ocean_freq", Settings.generation[(MapType.ocean, "freq")]);
			shader_gen.SetFloat("ocean_offset", Random.value);
			shader_gen.SetInt("ocean_details", 5);
			// Temperature
			shader_gen.SetFloat("temperature_min", -20);
			shader_gen.SetFloat("temperature_max", 30);
			shader_gen.SetFloat("temperature_roughness", .3f);
			shader_gen.SetFloat("temperature_freq", .0001f);
			shader_gen.SetFloat("temperature_offset", Random.value);
			shader_gen.SetInt("temperature_details", 4);
			// Humidity
			shader_gen.SetFloat("humidity_min", 0);
			shader_gen.SetFloat("humidity_max", 1);
			shader_gen.SetFloat("humidity_roughness", .4f);
			shader_gen.SetFloat("humidity_freq", .0002f);
			shader_gen.SetFloat("humidity_offset", Random.value);
			shader_gen.SetInt("humidity_details", 4);
			// Mountain
			shader_gen.SetFloat("mountain_min", 0);
			shader_gen.SetFloat("mountain_max", Settings.generation[(MapType.mountain, "max")]);
			shader_gen.SetFloat("mountain_roughness", Settings.generation[(MapType.mountain, "roughness")]);
			shader_gen.SetFloat("mountain_freq", Settings.generation[(MapType.mountain, "freq")]);
			shader_gen.SetFloat("mountain_offset", Random.value);
			shader_gen.SetInt("mountain_details", 4);
		}
		
		public (Texture2D, Texture2D) paintGPU(int2 pos, int size) {
			// pass the color texture
			var tex_color = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_color.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_color"), tex_color);
			// pass the height texture
			var tex_height = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_height.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_height"), tex_height);
			shader_h2n.SetTexture(kernel_h2n, Shader.PropertyToID("tex_height"), tex_height);
			// pass the normal texture
			var tex_normal = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_normal.Create();
			shader_h2n.SetTexture(kernel_h2n, Shader.PropertyToID("tex_normal"), tex_normal);
			// pass the info texture
			var tex_info = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_info.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_info"), tex_info);
			// pass the other parameters
			shader_h2n.SetInt("size", size);
			shader_gen.SetInt("x", pos.x*size);
			shader_gen.SetInt("y", pos.y*size);
			setStructGPU();
			// run overworld gen
			shader_gen.Dispatch(kernel_gen, size/Chunk.width, size/Chunk.width, 1);
			// run the h2n conversion
			shader_h2n.Dispatch(kernel_h2n, size/Chunk.width, size/Chunk.width, 1);
			// retrieve the color texture
			var color = new Texture2D(size, size, TextureFormat.RGBA32, true);
			RenderTexture.active = tex_color;
			color.ReadPixels(new Rect(0, 0, size, size), 0, 0);
			color.Apply();
			// retrieve the normal map
			var normal = new Texture2D(size, size, TextureFormat.RGB24, true);
			RenderTexture.active = tex_normal;
			normal.ReadPixels(new Rect(0, 0, size, size), 0, 0);
			normal.Apply();
			return (color, normal);
		}
		
		public Dictionary<int2, Chunk> get(int2 pos, int size) {
			var chunks = new Dictionary<int2, Chunk>();
			// pass the color texture
			var tex_color = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_color.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_color"), tex_color);
			// pass the height texture
			var tex_height = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_height.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_height"), tex_height);
			shader_h2n.SetTexture(kernel_h2n, Shader.PropertyToID("tex_height"), tex_height);
			// pass the normal texture
			var tex_normal = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_normal.Create();
			shader_h2n.SetTexture(kernel_h2n, Shader.PropertyToID("tex_normal"), tex_normal);
			// pass the info texture
			var tex_info = new RenderTexture(size, size, 32) {enableRandomWrite = true};
			tex_info.Create();
			shader_gen.SetTexture(kernel_gen, Shader.PropertyToID("tex_info"), tex_info);
			// pass the other parameters
			shader_h2n.SetInt("size", size);
			shader_gen.SetInt("x", pos.x*size);
			shader_gen.SetInt("y", pos.y*size);
			setStructGPU();
			// run overworld gen
			shader_gen.Dispatch(kernel_gen, size/Chunk.width, size/Chunk.width, 1);
			// run the h2n conversion
			shader_h2n.Dispatch(kernel_h2n, size/Chunk.width, size/Chunk.width, 1);
			// retrieve the info 
			var info = new Texture2D(size, size, TextureFormat.RGBA32, true);
			RenderTexture.active = tex_info;
			info.ReadPixels(new Rect(0, 0, size, size), 0, 0);
			info.Apply();
			for (int xchunk = 0; xchunk < size/Chunk.width; xchunk++) {
				for (int zchunk = 0; zchunk < size /Chunk.width; zchunk++) {
					var blocks = new BlockId[Chunk.width, Chunk.height, Chunk.width];
					for (int x = 0; x < Chunk.width; x++) {
						for (int z = 0; z < Chunk.width; z++) {
							Color c = info.GetPixel(xchunk*Chunk.width + x, zchunk*Chunk.width + z);
							float vOcean = c.r;
							float h = c.g;
							if (vOcean > 0) {
								int h_final = Math.Min(Math.Max((int) (h * 100.0), 0), Chunk.height - 1);
								blocks[x, h_final+1, z] = BlockId.grass_block;
							} else {
								blocks[x, 0, z] = BlockId.bedrock;
							}
						}
					}
					chunks.Add(pos + new int2(xchunk, zchunk), new Chunk(blocks));
				}
			}
			return chunks;
		}
	}
}
