﻿using Unity.Mathematics;

namespace Model {
	public class Chunk {
		public const int width = 16;
		public const int height = 128;
		public const int vol = width * width * height;
		
		private readonly BlockId[,,] _blocks;
		
		public Chunk(BlockId[,,] blocks) {
			_blocks = blocks;
		}
		
		public BlockId this[int3 pos] => _blocks[pos.x, pos.y, pos.z];
	}
}
