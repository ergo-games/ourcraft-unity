﻿using System.Collections;
using Model;
using Unity.Mathematics;
using UnityEngine;

namespace Scenes {
    public class ChunkTest : MonoBehaviour {
		[SerializeField] public int seed;
		[SerializeField] public float ocean_roughness = .3f;
		[SerializeField] public float ocean_freq = 0.00015f;
		[SerializeField] public float mountain_max = 1;
		[SerializeField] public float mountain_roughness = .4f;
		[SerializeField] public float mountain_freq = 0.0004f;
		[SerializeField] public GameObject quad;
		public int resolution = 528;
		private GenOverworld gen;
		
		void defaultSettings() {
			// settings
			Settings.seed = seed;
			Settings.bindings["forward"] = KeyCode.W;
			Settings.bindings["backward"] = KeyCode.S;
			Settings.bindings["left"] = KeyCode.A;
			Settings.bindings["right"] = KeyCode.D;
			Settings.bindings["jumping"] = KeyCode.Space;
			Settings.bindings["crouching"] = KeyCode.LeftShift;
			Settings.generation[(MapType.ocean, "roughness")] = ocean_roughness;
			Settings.generation[(MapType.ocean, "freq")] = ocean_freq;
			Settings.generation[(MapType.mountain, "max")] = mountain_max;
			Settings.generation[(MapType.mountain, "roughness")] = mountain_roughness;
			Settings.generation[(MapType.mountain, "freq")] = mountain_freq;
		}
		
		// Start is called before the first frame update
		private void Start() {
			defaultSettings();
			gen = new GenOverworld();
			for (int x = -2; x <= 2; x++) {
				for (int y = -1; y <= 1; y++) {
					GameObject q = Instantiate(quad, new Vector3(x+x*0.01f, y+y*0.01f, 0), Quaternion.identity);
					(Texture2D, Texture2D) color_normal = gen.paintGPU(new int2(x, y), resolution);
					q.GetComponent<MeshRenderer>().material.mainTexture = color_normal.Item1;
					q.GetComponent<MeshRenderer>().material.EnableKeyword("_NORMALMAP");
					q.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", color_normal.Item2);
				}
			}
		}
	}
}
