﻿using Model;
using UnityEngine;
using Unity.Mathematics;

namespace Scenes {
	public class BlockTest : MonoBehaviour {
		[SerializeField] public bool spectator = true;
		[SerializeField] public int renderDistance = 1;
		[SerializeField] public int seed;
		[SerializeField] public float ocean_roughness = .3f;
		[SerializeField] public float ocean_freq = 0.00015f;
		[SerializeField] public float mountain_max = 1;
		[SerializeField] public float mountain_roughness = .4f;
		[SerializeField] public float mountain_freq = 0.0004f;
		
		void defaultSettings() {
			// settings
			Settings.seed = seed;
			Settings.renderDistance = renderDistance;
			Settings.bindings["forward"] = KeyCode.W;
			Settings.bindings["backward"] = KeyCode.S;
			Settings.bindings["left"] = KeyCode.A;
			Settings.bindings["right"] = KeyCode.D;
			Settings.bindings["jumping"] = KeyCode.Space;
			Settings.bindings["crouching"] = KeyCode.LeftShift;
			Settings.generation[(MapType.ocean, "roughness")] = ocean_roughness;
			Settings.generation[(MapType.ocean, "freq")] = ocean_freq;
			Settings.generation[(MapType.mountain, "max")] = mountain_max;
			Settings.generation[(MapType.mountain, "roughness")] = mountain_roughness;
			Settings.generation[(MapType.mountain, "freq")] = mountain_freq;
		}
		
		void Start() {
			defaultSettings();
			Universe.worlds.Add(WorldType.overworld, new World(WorldType.overworld));
			Universe.worlds[WorldType.overworld].genChunks();
			Factories.Player.create(WorldType.overworld, new float3(2, 19, 2), spectator);
		}
	}
}
